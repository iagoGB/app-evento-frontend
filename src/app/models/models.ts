import { stringify } from '@angular/compiler/src/util';
import { throws } from 'assert';

export interface Event {
    "evento_id": number,
    "categoria_evento": number,
    "titulo": string,
    "localizacao": string,
    "palestrante": string[],
    "data_horario": Date,
    "carga_horaria": number,
}

export interface Categoria {
    "categoria_id": number,
    "nome": string
}

export class AuthUser {
    credential: number;
    password: string;
}

export interface Usuario {
    "usuario_id": number,
    "cpf": number,
    "nome": string
    "profissao": string,
    "carga_horaria": number,
    "data_ingresso": Date
}

export class EventoUsuario {

    nome: number;
    evento_id: number;
    usuario_id: number;
    isPresent: boolean;

    constructor(evento_id, usuario_id, isPresent, nome?) {

        this.evento_id = evento_id;
        this.usuario_id = usuario_id;
        this.isPresent = isPresent;

        if(!!nome){
            this.nome = nome;
        }

    }




}