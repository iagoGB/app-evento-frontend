import { HttpService } from 'src/app/services/http.service';
import { Location } from '@angular/common';
import { FormGroup, FormControl,Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { Usuario } from 'src/app/models/models';

@Component({
  selector: 'app-new-edit-user',
  templateUrl: './new-edit-user.page.html',
  styleUrls: ['./new-edit-user.page.scss'],
})
export class NewEditUserPage implements OnInit {
  
  formularioUsuario: FormGroup;
  private novoUsuario: Usuario;

  constructor (
    private http: HttpService,
    private location: Location,
    private toastController: ToastController
  ) { }

  ngOnInit() {

    this.formularioUsuario = new FormGroup({
      nome: new FormControl(null, Validators.required),
      cpf: new FormControl(null, Validators.required),
      profissao: new FormControl(null, Validators.required),
      data_ingresso: new FormControl(null, Validators.required)
    })
  }

  onSubmit(): void {
    if (this.formularioUsuario.status === "INVALID"){
      //Se o formulario estiver inválido, diga ao usuário
      console.log(this.formularioUsuario.status);
      console.log(this.formularioUsuario.value);
      console.log("Formulario invalido");
      this.presentToast("Formulário inválido, por favor preecha corretamente os campos",'danger');
    } 
    else {
      //Se o formulario estiver válido, mova para a variável evento 
      this.novoUsuario = this.formularioUsuario.value;
      //Envie para o servidor
      this.http.createUsuario(this.novoUsuario).subscribe(
      );
      console.log("novo Usuario:"+this.novoUsuario);
      //Informe ao usuário que o evento foi criado
      this.presentToast('Novo usuario criado!','dark');
      //Retorne pra tela de eventos
      this.location.back();
    
    }

  }

  //Feedback de situação para o usuário
  async presentToast(msg: string,c: string) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'middle',
      color: c,
      showCloseButton: true,
      closeButtonText:'x',
      duration: 2000
    });
    toast.present();
  }

}
