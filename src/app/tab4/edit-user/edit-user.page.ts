import { Router, RouterLink, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService } from 'src/app/services/http.service';
import { ToastController } from '@ionic/angular';
import { Usuario } from 'src/app/models/models';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.page.html',
  styleUrls: ['./edit-user.page.scss'],
})
export class EditUserPage implements OnInit {

  formularioUsuarioEdit: FormGroup;
  private editUsuario: Usuario;
  private esseid: number;

  constructor (
    private http: HttpService,
    private router: ActivatedRoute,
    private toastController: ToastController,
    private location: Location,
    
  ) { }

  ngOnInit() {

    this.router.params.subscribe (
      (params: any) => {
        this.esseid = params['id'];
        console.log(this.esseid);
        let event$ = this.http.getUserById(this.esseid);
        event$.subscribe (
          data => {
            this.editUsuario = data;
          }
        )
      }
    )
  

    this.formularioUsuarioEdit = new FormGroup({
      nome: new FormControl(null, Validators.required),
      cpf: new FormControl(null, Validators.required),
      profissao: new FormControl(null, Validators.required),
      data_ingresso: new FormControl(null, Validators.required)
    })

    this.updateForm();
  }

  onSubmit(): void {
    if (this.formularioUsuarioEdit.status === "INVALID"){
      //Se o formulario estiver inválido, diga ao usuário
      console.log(this.formularioUsuarioEdit.status);
      console.log(this.formularioUsuarioEdit.value);
      console.log("Formulario invalido");
      this.presentToast("Formulário inválido, por favor preecha corretamente os campos",'danger');
    } 
    else {
      //Se o formulario estiver válido, mova para a variável evento 
      this.editUsuario = this.formularioUsuarioEdit.value;
      //Envie para o servidor
      this.http.updateUsuario(this.editUsuario,this.editUsuario.usuario_id).subscribe(
      );
      console.log("novo Usuario:"+this.editUsuario);
      //Informe ao usuário que o evento foi criado
      this.presentToast('Usuário atualizado!','dark');
      //Retorne pra tela de eventos
      this.location.back();
    
    }

  }

  //Feedback de situação para o usuário
  async presentToast(msg: string,c: string) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'middle',
      color: c,
      showCloseButton: true,
      closeButtonText:'x',
      duration: 2000
    });
    toast.present();
  }

  updateForm(){
    this.formularioUsuarioEdit.get('nome').setValue(this.editUsuario.nome);
    this.formularioUsuarioEdit.get('cpf').setValue(this.editUsuario.cpf);
    this.formularioUsuarioEdit.get('profissao').setValue(this.editUsuario.profissao);
    this.formularioUsuarioEdit.get('data_ingresso').setValue(this.editUsuario.data_ingresso);

    console.log("Atualizou");
  }


}
