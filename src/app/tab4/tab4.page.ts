import { Component, OnInit } from '@angular/core';
import { Usuario } from '../models/models';
import { HttpService } from '../services/http.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {
  private ListaUsuario: Usuario[];
  
  
  constructor ( 
    private http: HttpService,
    private alertController: AlertController
  ) { }

  ngOnInit() {
     //Função para se inscrever na função http que requer atualização
     this.http.refreshNeeded().
     subscribe( () => { 
       this.getUsuario();
       console.log("fez o refresh");
     });
     this.getUsuario();
  }

  getUsuario(): void {
    this.http.getTodosUsuarios()
    .subscribe ( data => this.ListaUsuario = data );
  }

  //Evento assincrono para exibir modal ao clicar em deletar 
  async onDeleteConfirm ( usuario: Usuario ) {
    
    const alert = await this.alertController.create({
      header: 'Confirmar!',
      message: 'Tem certeza que deseja excluir o usuário: <strong>' + usuario.nome + '</strong>',
      buttons: [
        {
          //Botão de cancelar
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, 
        {
          //Botão de confirmar
          text: 'Sim',
          handler: ( ) => {
            this.remove(usuario.usuario_id);
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

  remove(id:number){
    return this.http.deleteUsuario(id).subscribe();
  }
}
