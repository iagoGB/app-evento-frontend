import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';

import { HttpService } from './../../services/http.service';
import { Event, Categoria } from './../../models/models';
import { ToastController } from '@ionic/angular';
import { FormBuilder, FormGroup, FormControl, Validators, FormArray } from '@angular/forms';

@Component({
  selector: 'app-new-event',
  templateUrl: './new-event.page.html',
  styleUrls: ['./new-event.page.scss'],
})
export class NewEventPage implements OnInit {

  formulario: FormGroup;
  public categorias: Categoria[];
  private t: string = '';
  public array = [
      "Teste",
      "Teste2",
      "Teste3"
  ]

  private categoria: Categoria = {
    categoria_id: null,
    nome: null
  }

  private newEvent: Event = {
    evento_id: null,
    categoria_evento: 5,
    titulo: "",
    localizacao: "",
    palestrante: [""],
    data_horario: null,
    carga_horaria: 0,
  }

  constructor (
    private http:HttpService, 
    private toastController:ToastController,
    private location: Location
  ) { }

  ngOnInit() {
    //Pega as categorias
    this.getCategoria();
    console.log(this.array);
    //Construção do formulário reativo
    this.formulario = new FormGroup({
      categoria_evento: new FormControl(null, Validators.required),
      titulo: new FormControl(null, Validators.required),
      localizacao: new FormControl(null, Validators.required),
      palestrante: new FormArray([
        new FormControl(null, Validators.required)
      ]),
      /*palestrante:new FormControl(null, Validators.required),*/
      data_horario: new FormControl(null, Validators.required),
      carga_horaria: new FormControl(null, Validators.required)
    })
  }
  

  //Metodo para pegar o Array no formulário
  get Palestrantes(): FormArray {
    return this.formulario.get('palestrante') as FormArray;
  }
  //Adicionar mais um campo para palestrante
  addPalestranteInput() {
    this.Palestrantes.push( new FormControl(null));
    console.log("Executou");
  }
  //Remover um campo de palestrante
  rmPalestranteInput(): void {
    this.Palestrantes.removeAt(this.Palestrantes.length-1);
  }

  //Função para criação do evento
  onSubmit(): void {
    if (this.formulario.status === "INVALID"){
      //Se o formulario estiver inválido, diga ao usuário
      console.log(this.formulario.status);
      console.log(this.formulario.value);
      console.log("Formulario invalido");
      this.presentToast("Formulário inválido, por favor preecha corretamente os campos",'danger');
    } 
    else {
      //Se o formulario estiver válido, mova para a variável evento 
      this.newEvent = this.formulario.value;
      /*
        this.newEvent.createdAt = this.newEvent.dataHorario;
        this.newEvent.updatedAt = this.newEvent.dataHorario;
      */
      //Envie para o servidor
      this.http.createEvent(this.newEvent).subscribe(
      );
      console.log("eventopreenchido:"+this.newEvent);
      //Informe ao usuário que o evento foi criado
      this.presentToast('Novo evento criado!','dark');
      //Retorne pra tela de eventos
      this.location.back();
      /* Falta incluir  travativa de erros */
    }

  }

  //Feedback de situação para o usuário
  async presentToast(msg: string,c: string) {
    const toast = await this.toastController.create({
      message: msg,
      position: 'middle',
      color: c,
      showCloseButton: true,
      closeButtonText:'x',
      duration: 2000
    });
    toast.present();
  }

  debug(){
    console.log(this.categorias);
  }

  getCategoria(){
    this.http.getCategoria().subscribe(data => this.categorias = data );
  }
}
