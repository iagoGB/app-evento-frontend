import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, RouterLink} from '@angular/router';
import { Location } from '@angular/common';
import { HttpService } from 'src/app/services/http.service';
import { Event } from 'src/app/models/models';

@Component({
  selector: 'app-detail-event',
  templateUrl: './detail-event.page.html',
  styleUrls: ['./detail-event.page.scss'],
})
export class DetailEventPage implements OnInit {

  public event: Event[];
  /*
  public event: Event = {
    evento_id : null,
    categoria_evento: 0,
    titulo: '',
    data_horario: null,
    localizacao: '',
    palestrante: [],
    carga_horaria: 0
  } */

  constructor (
    private http: HttpService,
    private router: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.router.params.subscribe (
      (params: any) => {
        let id = params['id'];
        console.log(id);
        let event$ = this.http.loadByID(id);
        event$.subscribe (
          data => {
            this.event = data;
          }
        )
      }
    )
  }
}
