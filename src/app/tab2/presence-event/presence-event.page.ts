import { Usuario, EventoUsuario } from './../../models/models';
import { ActivatedRoute } from '@angular/router';
import { HttpService } from './../../services/http.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-presence-event',
  templateUrl: './presence-event.page.html',
  styleUrls: ['./presence-event.page.scss'],
})
export class PresenceEventPage implements OnInit {

  private listaUsuario: EventoUsuario[]//Usuario[];
  private listaUsuarioEvento: EventoUsuario[];
  private evento_id: number;
  
  constructor(
    private http: HttpService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.route.params.subscribe (
      (params: any) => {
        //Guardando o id do evento
        this.evento_id = params['id'];
        console.log(this.evento_id);
    })

    this.getUsuario(this.evento_id);
  }

  getUsuario(event_id: number){
     this.http.getUsuario(event_id).subscribe(data => {
    
       this.listaUsuario = data
      
       this.listaUsuarioEvento = this.listaUsuario.map(usuario => new EventoUsuario(this.evento_id,usuario.usuario_id,false,usuario.nome));

     });

   }

  registerUsers(){

    this.http.registerEventUsers(this.evento_id,this.listaUsuario).subscribe();
    /*.then(
      () => {

        console.log("foi");

      }
    )*/

  }

  /*
  getUsuario(eventoId: number){
    this.http.getUsuario(eventoId).subscribe(data => this.listaUsuario = data);
  } */

}
