import { Event, Categoria, Usuario, EventoUsuario } from './../models/models';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, Subject } from 'rxjs';
import { take, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})


export class HttpService {

  private url: string = "http://localhost:8090/eventos";
  private url_1: string = "http://localhost:8090/usuarios";
  private url_2: string = "http://localhost:8090/categoria";
  private url_3: string = "http://localhost:8090/usuario-evento";
  private url_4: string = "http://localhost:8090/eventos/deletar";
  private url_5: string = "http://localhost:8090/usuario-evento/register";
  //private events: string = "/events";
  private _refreshNeeded: Subject<void> = new Subject<void>();

  constructor(private http: HttpClient) {
    
  };

  //Método para pegar um único evento
  loadByID(evento_id:number):any {
    return this.http.get(this.url+"/"+evento_id);
  }

  //Método para pegar array de categoria no servidor
  getCategoria():any{
    return this.http.get(this.url_2).pipe(take(1));
  }

  //Método para pegar array de evento no servidor
  getEvents():Observable<Event[]>{
    return this.http.get<Event[]>(this.url);
  }
  //Método para pegar array de usuários em um evento no servidor
  getUsuario(eventoId: number):Observable<EventoUsuario[]>{
    return this.http.get<EventoUsuario[]>(this.url_3+"/"+eventoId);
  }

  createUsuario(novoUsuario:Usuario){
    return this.http.post(this.url_1+"/criar/",novoUsuario)
    .pipe(
      take(1),
      tap(() => {
          this._refreshNeeded.next();
      })
    );
  }

  getUserById(id: number): any{
    return this.http.get(this.url_1+"/"+id)
  }

  updateUsuario(editUsuario: Usuario, id){
    return this.http.put(this.url_1+"/editar/"+id,editUsuario)
    .pipe(
      take(1),
      tap(() => {
          this._refreshNeeded.next();
      })
    );
  }

  deleteUsuario(usuario_id:number){
    return this.http.delete(this.url_1+"/deletar/"+usuario_id)
    .pipe(
      take(1),
      tap (
        () => {
          this._refreshNeeded.next();
        }
      )
    );
  }

  getTodosUsuarios(){
    return this.http.get<Usuario[]>(this.url_1);
  };

  // Requisição para o servidor criar novo registro
  createEvent(newEvent: Event){
    console.log("Variavel que está sendo mandanda:"+ newEvent);
    return this.http.post(this.url+"/novo",newEvent)
    .pipe(
      take(1),
      tap(() => {
          this._refreshNeeded.next();
      })
    );
    //pipe take 1 Faz a requisição apenas uma única vez e encerra o observable automaticamente
  }
  
  registerEventUsers(event_id,usersEvent){

    let url = `${this.url_5}/${event_id}`;

   /* return this.http.post(url, JSON.stringify(usersEvent), { headers:new HttpHeaders({ 'Content-type':'application/json' })}).toPromise();*/
    return this.http.post(this.url_3+"/register/"+event_id,usersEvent);
  }

  //Retorna a variavel responsável por fazer o refresh
  refreshNeeded(): Subject<any> {
    return this._refreshNeeded;
  }
  
  // Requisição para o servidor atualizar registro
  updateEvent(toUpdateEvent: Event) {
    return this.http.put(this.url+'/'+toUpdateEvent.evento_id,toUpdateEvent)
    .pipe(
      take(1),
      tap(() => {
          this._refreshNeeded.next();
      })
    );
    //pipe take 1 - Faz a requisição apenas uma única vez e encerra o observable automaticamente
  }

  // Requisição para o servidor deletar registro
  removeEvent(toDeleteEvent: Event){
    return this.http.delete(this.url_4+'/'+toDeleteEvent.evento_id)
    .pipe(
      take(1),
      tap (
        () => {
          this._refreshNeeded.next();
        }
      )
    );
    //pipe take 1 - Faz a requisição apenas uma única vez e encerra o observable automaticamente
  }
 

}
